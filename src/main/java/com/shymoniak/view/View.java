package com.shymoniak.view;

import com.shymoniak.model.*;
import com.shymoniak.model.parser.GSON;
import com.shymoniak.model.parser.ValidatorJSON;

import java.util.Scanner;

public class View {
    MainModel mainModel = new MainModel();
    FileCreator fileCreator = new FileCreator();

    public void startApp() {
        fileCreator.createIfNotExistFile(Defaults.JSON_FILE);
        fileCreator.createIfNotExistFile(Defaults.JSON_SCHEMA);

        System.out.println("Choose a command:");
        System.out.println("1. Validate JSON file");
        System.out.println("2. Read + Write using GSON");
        System.out.println("3. Read + Write using Jackson");
        System.out.println("4. Print sorted list of candies");
        System.out.println("9. Exit");

        Scanner scan = new Scanner(System.in);
        int var = scan.nextInt();
        while (var != 9){
            if (var == 1) validate();
            else if (var == 2) useGSON();
            else if (var == 3) useJackson();
            else if (var == 4) mainModel.printSorted(new GSON().readFromFile(Defaults.JSON_FILE));
            else if (var == 9) break;

            var = scan.nextInt();
        }
    }

    public void validate() {
        if (Defaults.JSON_FILE.length() != 0) {
            System.out.println("JSON is valid: "
                    + new ValidatorJSON().validate(Defaults.JSON_FILE, Defaults.JSON_SCHEMA));
        } else {
            System.err.println("File is empty");
        }
    }

    public void useGSON() {
        mainModel.writeUsingGSON();
        mainModel.readUsingGSON();
    }

    public void useJackson() {
        mainModel.wrineUsingJackson();
        mainModel.readUsingJackson();
    }

}
