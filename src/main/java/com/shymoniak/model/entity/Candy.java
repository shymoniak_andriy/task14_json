package com.shymoniak.model.entity;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Candy {
    private String name;
    private int calories;
    private Type type;
    private List<Ingredient> ingredients;
    private Production production;
}