package com.shymoniak.model.entity;

import lombok.*;

@Getter
@ToString
@NoArgsConstructor
public enum Production {
    ROSHEN("ROSHEN"), AVK("AVK"), SVITOCH("SVITOCH"), WILLY_WONKA("WILLY_WONKA");

    private String prodName;

    Production(String prodName) {
        this.prodName = prodName;
    }
}