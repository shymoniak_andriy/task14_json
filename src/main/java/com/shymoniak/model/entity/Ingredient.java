package com.shymoniak.model.entity;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ingredient {
    private String ingredientName;
    private int quality;
}
