package com.shymoniak.model.entity;

import java.util.Comparator;

public class CandyComparator implements Comparator<Candy> {


    @Override
    public int compare(Candy o1, Candy o2) {
        int res = o1.getName().compareToIgnoreCase(o2.getName());
        if (res == 0) {
            res = Integer.compare(o1.getCalories(), o2.getCalories());
        }
        return res;
    }
}
