package com.shymoniak.model;

import java.io.File;
import java.io.IOException;

public class FileCreator {

    public void createIfNotExistFile(File file) {
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
