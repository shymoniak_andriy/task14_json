package com.shymoniak.model;

import com.shymoniak.model.entity.Candy;
import com.shymoniak.model.entity.Ingredient;
import com.shymoniak.model.entity.Production;
import com.shymoniak.model.entity.Type;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Defaults {
    public static final String FILE_PATH = "D:" + File.separator + "EPAM" + File.separator + "Homework"
            + File.separator + "homework17(JSON)" + File.separator + "task14_json" + File.separator + "src"
            + File.separator + "main" + File.separator + "resources" + File.separator;
    public static final File JSON_FILE = new File(FILE_PATH + "candies.json");
    public static final File JSON_SCHEMA = new File(FILE_PATH + "candiesSchema.json");

    public List<Candy> getCandyList() {
        List<Candy> candies = new ArrayList<>();
        List<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(new Ingredient("Chocolate beans", 8));
        ingredients.add(new Ingredient("Nuts", 7));
        ingredients.add(new Ingredient("Sugar", 9));
        candies.add(new Candy("Шоколапки", 463, Type.NOUGAT, ingredients, Production.ROSHEN));
        candies.add(new Candy("Ромашка", 440, Type.GLAZE, ingredients, Production.ROSHEN));
        candies.add(new Candy("Монблан", 580, Type.CHOCOLATE, ingredients, Production.ROSHEN));
        candies.add(new Candy("Шоколад з манго", 600, Type.CHOCOLATE, ingredients, Production.AVK));
        candies.add(new Candy("Шоколад з горіхом", 600, Type.CHOCOLATE, ingredients, Production.AVK));
        candies.add(new Candy("Nutty Crunch Surprise", 550, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Chilly Chocolate Creme", 503, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Triple Dazzle Caramel", 550, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Whipple Scrumptious Fudgemallow Delight", 580, Type.CHOCOLATE, ingredients, Production.WILLY_WONKA));
        candies.add(new Candy("Червоний мак", 540, Type.CHOCOLATE, ingredients, Production.SVITOCH));
        candies.add(new Candy("Зоряне сяйво", 490, Type.CHOCOLATE, ingredients, Production.SVITOCH));
        return candies;
    }
}
