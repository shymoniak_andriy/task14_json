package com.shymoniak.model.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shymoniak.model.entity.Candy;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class GSON {
    private static final Gson GSON = new Gson();
    private static final GsonBuilder GSON_BUILDER = new GsonBuilder();


    public void writeIntoFile(File file, List<Candy> candies) {
        try (FileWriter fileWriter = new FileWriter(file.getPath())) {
            GSON_BUILDER.setPrettyPrinting().create().toJson(candies, fileWriter);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<Candy> readFromFile(File file) {
        List<Candy> candies = null;
        try (FileReader fileReader = new FileReader(file.getPath())) {
            candies = Arrays.asList(GSON.fromJson(fileReader, Candy[].class));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return candies;
    }
}