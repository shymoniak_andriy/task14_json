package com.shymoniak.model.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class ValidatorJSON {

    public boolean validate(File json, File schema) {
        final JsonNode jsonNode;
        final JsonNode schemaNode;
        final JsonSchemaFactory schemaFactory;
        ProcessingReport processingReport = null;
        try {
            jsonNode = JsonLoader.fromFile(json);
            schemaNode = JsonLoader.fromFile(schema);
            schemaFactory = JsonSchemaFactory.byDefault();
            JsonValidator jsonValidator = schemaFactory.getValidator();
            processingReport = jsonValidator.validate(schemaNode, jsonNode);
        } catch (IOException ex){
            ex.printStackTrace();
        } catch (ProcessingException ex){
            ex.printStackTrace();
        }
        return processingReport.isSuccess();
    }
}
