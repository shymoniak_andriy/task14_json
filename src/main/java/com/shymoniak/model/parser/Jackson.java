package com.shymoniak.model.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shymoniak.model.Defaults;
import com.shymoniak.model.entity.Candy;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Jackson {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public void writeIntoFile(File file, List<Candy> candies){
        try (FileWriter fileWriter = new FileWriter(Defaults.JSON_FILE)){
            OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValue(file, candies);
        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public List<Candy> readFromFile(File file) {
        List<Candy> candies = new ArrayList<>();
        try {
            candies = Arrays.asList(OBJECT_MAPPER.readValue(file, Candy[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return candies;
    }
}
