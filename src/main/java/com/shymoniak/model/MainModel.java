package com.shymoniak.model;

import com.shymoniak.model.entity.Candy;
import com.shymoniak.model.entity.CandyComparator;

import java.util.*;

/**
 * 1. Створити файл JSON і відповідну йому схему JSON Schema.
 * 2. При розробленні JSON Schema використовувати якомога більше
 * перевірок (JSON Schema Validators).
 * 3. Написати Java-клас, що відповідає поданому описанню.
 * 4. Програма також повинна містити окремий клас для опрацювання JSON
 * об’єктів (використовувати Jackson або будь яку іншу бібліотеку).
 * 5. Перевірити коректність JSON-документа з використанням JSON Schema.
 * 6. Для сортування об’єктів використовувати інтерфейс Comparator.
 */
public class MainModel {
    GSON gson = new GSON();
    Jackson jackson = new Jackson();
    Defaults defaults = new Defaults();


    public void writeUsingGSON() {
        gson.writeIntoFile(Defaults.JSON_FILE, defaults.getCandyList());
    }

    public void readUsingGSON() {
        List<Candy> candies = gson.readFromFile(Defaults.JSON_FILE);
        printList(candies);
    }

    public void wrineUsingJackson() {
        jackson.writeIntoFile(Defaults.JSON_FILE, defaults.getCandyList());
    }

    public void readUsingJackson() {
        List<Candy> candies = jackson.readFromFile(Defaults.JSON_FILE);
        printList(candies);
    }

    public void printList(List<Candy> candies) {
        candies.stream().forEach(candy -> System.out.println(candy.toString()));
    }

    public void printSorted(List<Candy> candies){
        Collections.sort(candies, new CandyComparator());
        printList(candies);
    }
}
